package de.uni_hamburg.informatik.swt.se2.kino.werkzeuge.Barzahlung;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.plaf.DimensionUIResource;

public class BarzahlungUI 
{
	private static final String ZU_ZAHLEN = "Zu Bezahlender Betrag";
	
	private static final  String GEZAHLTER_BETRAG = "Gezahlter Betrag";
	
	private static final String RESTBETRAG = "Das Rückgeld";
	
	private JFrame _window;
	
	private JLabel _zuZahlen;
	
	private JLabel _gezahleterBetrag;
	
	private JLabel _restbetrag;
	
	private JPanel _okPanel;
	
	private JPanel _abbrechenPanel;
	
	private JButton _okButton;
	
	private JButton _abbrechenButton;
	
	private JTextField _gezahlterBetragEingabe; 
	
	public BarzahlungUI()
	{
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
    	_window  = new JFrame("Barzahlung");
    	_window.setSize(400, 400);
    	_window.setLocation((size.width / 2) - (400 / 2), (size.height / 2) - (400 / 2));
    	
    	
		createPanels();
		createButton();
		createLabel();
		createTextField();
		//_window.pack();
		_window.setVisible(true);
	}
	
	private void createPanels()
	{
		_okPanel = new JPanel();
		_abbrechenPanel = new JPanel();
		
		_window.add(_okPanel);
		_window.add(_abbrechenPanel);
	}
	
	private void createButton()
	{
		_okButton = new JButton("Ok");
		_abbrechenButton = new JButton("Abbrechen");
	}
	
	private void createLabel()
	{
		_zuZahlen = new JLabel(ZU_ZAHLEN);
		_zuZahlen.setFont(new Font("Arial",Font.BOLD, 20));
		_gezahleterBetrag = new JLabel(GEZAHLTER_BETRAG);
		_gezahleterBetrag.setFont(new Font("Arial",Font.BOLD, 20));
		_restbetrag = new JLabel(RESTBETRAG);
		_restbetrag.setFont(new Font("Arial",Font.BOLD, 20));
	}
	
	private void createTextField()
	{
		_gezahlterBetragEingabe = new JTextField();
	}
}
